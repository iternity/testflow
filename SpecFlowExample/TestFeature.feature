﻿@regression
Feature: TestFeature
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@important
Scenario: Add two numbers
	Given I have entered <op1> into the calculator
	And I have entered <op2> into the calculator
	When I press add
	Then the result should be <result> on the screen
Examples: 
| op1 | op2 | result |
| 70  | 50  | 120    |
| 50  | 70  | 120    |
| 50  | 0   | 50     |

Scenario: Multiply two numbers
	Given I have entered 6 into the calculator
	And I have entered 20 into the calculator
	When I press multiply
	Then the result should be 120 on the screen

@v1.1.0 @longrunning
Scenario: Subtract two numbers
	Given I have entered 70 into the calculator
	And I have entered 50 into the calculator
	When I press subtract
	Then the result should be 20 on the screen

Scenario: Divide two numbers
	Given I have entered 120 into the calculator
	And I have entered 2 into the calculator
	When I press divide
	Then the result should be 60 on the screen