﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TechTalk.SpecFlow;

namespace SpecFlowExample
{
    [Binding]
    public class TestFeatureSteps
    {
        private ScenarioContext scenarioCtx;

        public TestFeatureSteps(ScenarioContext sc)
        {
            this.scenarioCtx = sc;
        }

        [Given(@"I have entered (.*) into the calculator")]
        public void GivenIHaveEnteredIntoTheCalculator(int p0)
        {
            // Weird: With Assert.Pass() the whole scenario passes instantaneously without executing the subsequent steps  
            // Maybe be a specflow bug?
            // An Assert.True(true) behaves as expected.
        }

        [When(@"I press add")]
        public void WhenIPressAdd()
        {
            // This step is automated but fails.
            Assert.Fail();
        }
        
        [When(@"I press subtract")]
        public void WhenIPressSubtract()
        {
            // This step is not yet implemented or needs a human tester.
            scenarioCtx.Pending();
        }

        [When(@"I press multiply")]
        public void WhenIPressMultiply()
        {
            // This step is automated but throws an explicit exception.
            throw new Exception("Something bad happened");    
        }

        [When(@"I press divide")]
        public void WhenIPressDivide()
        {
            // This step is automated and everything works fine. No assertion, no exception.
        }

        [Then(@"the result should be (.*) on the screen")]
        public void ThenTheResultShouldBeOnTheScreen(int p0)
        {
            // This step is automated and passes.
            Assert.IsTrue(true);
        }
    }
}
