﻿
namespace Iternity.TestFlow.Zephyr.Internal
{

    /// <summary>
    /// JIRA API Endpoints
    /// </summary>
    internal static class JAPIEndpoints
    {
        public static string JIRA_AddIssueURL = "rest/api/2/issue/";
        public static string JIRA_IssueListURL = @"rest/api/2/search?jql=project={0} and summary~""{1}""&maxResults=1&startAt=0";
    }

    /// <summary>
    /// Zephyr API "ZAPI" Endpoints
    /// </summary>
    internal static class ZAPIEndpoints
    {
        public static string JIRA_ExecutionDetailURL = "rest/zapi/latest/execution?issueId=";
        public static string JIRA_CycleDetailURL = "rest/zapi/latest/cycle?projectId={0}&versionId={1}";
        public static string JIRA_AddIssuetoExecutionURL = "rest/zapi/latest/execution/";
        public static string JIRA_TestStepDetailsUpdate = "rest/zapi/latest/teststep/";
        public static string JIRA_GETStepsResult = "rest/zapi/latest/stepResult?executionId=";
        public static string JIRA_UpdateTestStepResult = "rest/zapi/latest/stepResult/";
        public static string JIRA_ExecutionStatusUpdate = "rest/zapi/latest/execution/{0}/execute";
    }

    internal static class ZAPITestExecutionStatus
    {
        public static readonly int UNEXECUTED = -1;
        public static readonly int PASS = 1;
        public static readonly int FAIL = 2;
        public static readonly int WIP = 3;
        public static readonly int BLOCKED = 4;
        public static readonly int PENDING = 5;
        public static readonly int UNDEFINED = 6;

        public static int GetStatus(TestExecutionStatus status)
        {
            switch (status)
            {
                case TestExecutionStatus.Unexecuted:    return UNEXECUTED;
                case TestExecutionStatus.Pass:          return PASS;
                case TestExecutionStatus.Fail:          return FAIL;
                default:                                return UNEXECUTED;
            }
        }
    }

    internal static class ZAPITestStepExecutionStatus
    {
        public static readonly int UNEXECUTED = -1;
        public static readonly int PASS = 1;
        public static readonly int FAIL = 2;
        public static readonly int WIP = 3;
        public static readonly int BLOCKED = 4;

        public static int GetStatus(TestExecutionStatus status)
        {
            switch (status)
            {
                case TestExecutionStatus.Unexecuted:    return UNEXECUTED;
                case TestExecutionStatus.Pass:          return PASS;
                case TestExecutionStatus.Fail:          return FAIL;
                default:                                return UNEXECUTED;
            }
        }
    }

    

}
