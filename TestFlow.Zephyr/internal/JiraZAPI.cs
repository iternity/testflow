﻿/// <summary>
/// This is an internal class providing various low level ZAPI calls,
/// to create and update Zephyr Tests and TestSteps and their respective Executions. 
/// </summary>
/// <author>Aress Inc. / iTernity GmbH</author>

using Iternity.TestFlow.Zephyr.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.Threading.Tasks;

namespace Iternity.TestFlow.Zephyr.Internal
{
    // TODO This is still mainly PoC code and should be refactored.
    internal class JiraZAPI
    {
        #region "Internal API"

        /// <summary>
        /// This method is used for fetching a JIRA/Zephyr test case with the given <paramref name="summary"/>.
        /// Only the project specified by <paramref name="ctx"/> is searched in.
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="summary"></param>
        /// <returns> It will return the first test case that matches <paramref name="summary"/></returns>
        internal async static Task<TestCaseModel> GetTestCase(ZephyrContext ctx, string summary)
        {
            string absoluteUrl = string.Format(ctx.GetAbsUrl(JAPIEndpoints.JIRA_IssueListURL), ctx.ProjectId, summary);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(absoluteUrl);
            request.Method = "GET";
            request.ContentType = request.MediaType = "application/json";
            request.Headers.Add("Authorization", string.Concat("Basic ", ctx.GetBase64BasicAuth()));
            try
            {
                WebResponse webResponse = await request.GetResponseAsync();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                string response = responseReader.ReadToEnd();
                TestCaseModel jsonresult = JsonConvert.DeserializeObject<TestCaseModel>(response);
                return jsonresult;

            }
            catch (Exception)
            {
                return new TestCaseModel();
            }
        }

        /// <summary>
        /// This method is used for updating scenarios status
        /// </summary>
        /// <param name="id"></param>
        /// <param name="executionStatus"></param>
        /// <param name="executionComment"></param>
        /// <param name="ctx"></param>
        /// <returns>It will returs true if status are updated sucessfully</returns>

        internal async static Task<bool> UpdateExecutionStatus(string id, int executionStatus, string executionComment, ZephyrContext ctx)
        {
            var absoluteUrl = string.Format(ctx.GetAbsUrl(ZAPIEndpoints.JIRA_ExecutionStatusUpdate), id);
            byte[] postData = null;
            if (!string.IsNullOrEmpty(absoluteUrl))
            {
                var request = WebRequest.Create(absoluteUrl) as HttpWebRequest;
                if (request != null)
                {
                    request.KeepAlive = true;
                    var cachePolicy = new RequestCachePolicy(RequestCacheLevel.BypassCache);
                    request.CachePolicy = cachePolicy;
                    request.Expect = null;
                    request.Method = "PUT";

                    request.ContentType = request.MediaType = "application/json";
                    request.Headers.Add("Authorization", string.Concat("Basic ", ctx.GetBase64BasicAuth()));

                    //Create json string for add bug to execution
                    dynamic jsonObject = new JObject();
                    jsonObject.status = executionStatus;
                    jsonObject.comment = executionComment;
                    string jsons = JsonConvert.SerializeObject(jsonObject, Formatting.Indented);
                    postData = StringToByteArray(jsons);

                    if (postData != null)
                    {
                        request.ContentLength = postData.Length;
                        using (var dataStream = request.GetRequestStream())
                        {
                            dataStream.Write(postData, 0, postData.Length);
                        }
                    }

                    try
                    {
                        using (var httpWebResponse = await request.GetResponseAsync() as HttpWebResponse)
                        {
                            if (httpWebResponse != null)
                            {
                                using (var streamReader = new StreamReader(httpWebResponse.GetResponseStream()))


                                    return true;
                            }
                        }
                    }
                    catch (Exception) { return false; }
                }
            }
            return false;
        }

        /// <summary>
        /// This method is used for finding the test execution id for specific test case in a release cycle.
        /// Following scenarios are covered -
        /// 1. Create issue if not exists 
        /// 2. Add issue to release cycle
        /// 3. Fetch execution id
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="summary"></param>
        /// <param name="testStepDetails"></param>
        /// <param name="labels"></param>
        /// <returns>It will return step execution object.</returns>
        internal static ExecutionDetails EnsureTestStepExecution(ZephyrContext ctx, string summary, string testStepDetails, params string[] labels)
        {
            int cycleid = Task.Run(() => FetchCycleId(ctx)).Result;
            int issueid = FindIssueId(ctx, summary);
            if (issueid == 0) //If issue id not present
            {
                // Create Issue (Zephyr Test)
                issueid = CreateIssue(summary, ctx, labels);
                
                // Ensure that the Teststep exists
                int stepId = EnsureTestStep(testStepDetails, issueid, ctx);

                // Add the test execution to the cycle
                return AddTestExecution(ctx, cycleid, issueid, stepId);
            }
            else //If issue id is present
            {
                //Init Execution details
                Executions executions = Task.Run(() => FindExecutionsDetails(Convert.ToString(issueid), ctx.TestCycleName, ctx.ReleaseName, ctx)).Result;
                if (executions == null)
                {
                    executions = new Executions();
                }

                // Load all test steps of this issue and update the IssueStepDetailsModel
                if (!ctx.IssueStepDetailsModel.ContainsKey(issueid))
                {
                    TestSteps lstStepDetails = Task.Run(() => GetIssueTestSteps(issueid, ctx)).Result;
                    ctx.IssueStepDetailsModel.Add(issueid, lstStepDetails.stepDetails);
                }

                // Get first stepId that matches the testStepDetails
                int stepId = FindMatchingStepInContext(ctx, testStepDetails, issueid);

                // Add the test step if it's not present
                if (stepId == 0)
                {
                    stepId = AddTestStep(ctx, issueid, testStepDetails);
                }

                // Return an existing ExecutionDetails ...
                if (executions.id == 0)
                {
                    return AddTestExecution(ctx, cycleid, issueid, stepId);
                }
                // ... or create a new one, if no execution exists.
                return new ExecutionDetails()
                {
                    stepId = stepId,
                    issueId = issueid,
                    executionId = executions.id,
                };
            }
        }

        /// <summary>
        /// This method is used for adding or updating step result.
        /// Following scenarios are covered -
        /// 1. Add step result 
        /// 2. Update step result in case result already exists
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="stepExecution"></param>
        /// <param name="status"></param>
        /// <param name="result"></param>
        /// <returns>It will returs true if results are updated sucessfully</returns>

        internal async static Task<bool> UpdateTestStepResult(ZephyrContext ctx, ExecutionDetails stepExecution, int status, string result)
        {
            var absoluteUrl = ctx.GetAbsUrl(ZAPIEndpoints.JIRA_UpdateTestStepResult);
            if (!string.IsNullOrEmpty(absoluteUrl))
            {
                byte[] postData = null;
                HttpWebRequest request = WebRequest.Create(absoluteUrl) as HttpWebRequest;
                if (request != null)
                {
                    request.KeepAlive = true;
                    var cachePolicy = new RequestCachePolicy(RequestCacheLevel.BypassCache);
                    request.CachePolicy = cachePolicy;
                    request.Expect = null;
                    request.Method = "POST";

                    request.ContentType = request.MediaType = "application/json";
                    request.Headers.Add("Authorization", string.Concat("Basic ", ctx.GetBase64BasicAuth()));

                    //Create json string for add Test Step Details
                    dynamic jsonsString = new JObject();
                    jsonsString.executionId = stepExecution.executionId;
                    jsonsString.stepId = stepExecution.stepId;
                    jsonsString.issueId = stepExecution.issueId;
                    jsonsString.status = status;
                    jsonsString.comment = result;
                    string teststepjson = JsonConvert.SerializeObject(jsonsString, Formatting.Indented);
                    postData = StringToByteArray(teststepjson);


                    if (postData != null)
                    {
                        request.ContentLength = postData.Length;
                        using (var dataStream = request.GetRequestStream())
                        {
                            dataStream.Write(postData, 0, postData.Length);
                        }
                    }
                    try
                    {
                        using (var httpWebResponse = await request.GetResponseAsync() as HttpWebResponse)
                        {
                            if (httpWebResponse != null)
                            {
                                using (var streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                                {
                                    result = streamReader.ReadToEnd();
                                    return true;
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {

                        List<StepsResult> stepsExecution = Task.Run(() => GetStepsResult(stepExecution.executionId, ctx)).Result;
                        if (stepsExecution.Count != 0)
                        {
                            StepsResult stepResult = stepsExecution.Where(s => s.stepId == stepExecution.stepId).FirstOrDefault();

                            //Create json string for add Test Step Details
                            dynamic jsonString = new JObject();
                            jsonString.id = stepResult.id;
                            jsonString.issueId = stepExecution.issueId;
                            jsonString.executionId = stepExecution.executionId;
                            jsonString.status = status;
                            jsonString.comment = result;
                            string stepjson = JsonConvert.SerializeObject(jsonString, Formatting.Indented);
                            postData = StringToByteArray(stepjson);

                            return Task.Run(() => UpdateStepsResult(ctx, stepResult.id, postData)).Result;
                        }

                        return false;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// This method is used to reset all steps of an already existing test execution
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="stepExecution"></param>
        /// <returns>It will returs true if results are updated sucessfully</returns>

        internal static async Task<bool> ResetAllTestStepResults(ZephyrContext ctx, ExecutionDetails stepExecution)
        {
            bool success = true;
            List<StepsResult> stepsExecution = Task.Run(() => GetStepsResult(stepExecution.executionId, ctx)).Result;
            foreach(var stepResult in stepsExecution)
            {
                //Create json string for add Test Step Details
                dynamic jsonString = new JObject();
                jsonString.id = stepResult.id;
                jsonString.issueId = stepExecution.issueId;
                jsonString.executionId = stepExecution.executionId;
                jsonString.status = ZAPITestStepExecutionStatus.UNEXECUTED;
                jsonString.comment = "";
                string stepjson = JsonConvert.SerializeObject(jsonString, Formatting.Indented);
                var postData = StringToByteArray(stepjson);

                success = success && await UpdateStepsResult(ctx, stepResult.id, postData);
            }

            return success;

        }

        /// <summary>
        /// This method is used for updating specific issue steps.
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="stepId"></param>
        /// <param name="postData"></param>
        /// <returns>It will return true on success else false.</returns>

        internal async static Task<bool> UpdateStepsResult(ZephyrContext ctx, int stepId, byte[] postData)
        {
            var relativeUrl = ZAPIEndpoints.JIRA_UpdateTestStepResult;
            var absoluteUrl = string.Concat(ctx.GetAbsUrl(relativeUrl), Convert.ToString(stepId));
            if (!string.IsNullOrEmpty(absoluteUrl))
            {
                var request = WebRequest.Create(absoluteUrl) as HttpWebRequest;
                if (request != null)
                {
                    request.KeepAlive = true;
                    var cachePolicy = new RequestCachePolicy(RequestCacheLevel.BypassCache);
                    request.CachePolicy = cachePolicy;
                    request.Expect = null;
                    request.Method = "PUT";

                    request.ContentType = request.MediaType = "application/json";
                    request.Headers.Add("Authorization", string.Concat("Basic ", ctx.GetBase64BasicAuth()));

                    if (postData != null)
                    {
                        request.ContentLength = postData.Length;
                        using (var dataStream = request.GetRequestStream())
                        {
                            dataStream.Write(postData, 0, postData.Length);
                        }
                    }
                    try
                    {
                        using (var httpWebResponse = await request.GetResponseAsync() as HttpWebResponse)
                        {
                            if (httpWebResponse != null)
                            {
                                using (var streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                                {
                                    string response = streamReader.ReadToEnd();
                                    return true;
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {

                        return false;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Ensure that there is Test execution present.
        /// Procedure:
        /// - Try to find the first Zephyr Test issue with this summary.
        /// - If no issue exists create one. 
        /// - Find a test execution for this issue for the current cycle of the version under test.
        /// - If no test execution exists, create one.
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="summary"></param>
        /// <param name="labels"></param>
        /// <returns></returns>
        internal static ExecutionDetails EnsureTestExecution(ZephyrContext ctx, string summary, params string[] labels)
        {
            int issueid = FindIssueId(ctx, summary);
            if (issueid == 0) //If issue id not present
            {
                // Create Issue (Zephyr Test)
                issueid = CreateIssue(summary, ctx, labels);
            }

            Executions executions = Task.Run(() => FindExecutionsDetails(Convert.ToString(issueid), ctx.TestCycleName, ctx.ReleaseName, ctx)).Result;
            if (executions == null || executions.id == 0)
            {
                var cycleid = Task.Run(() => FetchCycleId(ctx)).Result;
                return AddTestExecution(ctx, cycleid, issueid);
            }
            return new ExecutionDetails()
            {
                issueId = issueid,
                executionId = executions.id,
            };
        }
        #endregion



        #region "Privates"

        /// <summary>
        /// Get first stepId that matches the testStepDetails
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="testStepDetails"></param>
        /// <param name="issueid"></param>
        /// <returns>0 if no matching step could be found</returns>
        private static int FindMatchingStepInContext(ZephyrContext ctx, string testStepDetails, int issueid)
        {
            int stepId = 0;
            if (ctx.IssueStepDetailsModel.ContainsKey(issueid))
            {
                List<StepDetails> lst = ctx.IssueStepDetailsModel[issueid];
                try
                {
                    stepId = lst.Where(s => s.stepDetails == testStepDetails).Select(s => s.stepId).FirstOrDefault();
                }
                catch (Exception) { }
            }

            return stepId;
        }

        /// <summary>
        /// Add a new TestStep in Zephyr and update the IssueStepDetailsModel for this <paramref name="issueid"/> in the <paramref name="ctx"/>
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="issueid"></param>
        /// <param name="testStepDetails"></param>
        /// <returns></returns>
        private static int AddTestStep(ZephyrContext ctx, int issueid, string testStepDetails)
        {
            //Create json string for add Test Step Details
            dynamic jsonsObject = new JObject();
            jsonsObject.step = testStepDetails;
            jsonsObject.data = string.Empty;
            jsonsObject.result = string.Empty;
            string teststepjson = JsonConvert.SerializeObject(jsonsObject, Formatting.Indented);

            StepDetails result = Task.Run(() => AddTestDetails(issueid, StringToByteArray(teststepjson), ctx)).Result;
            if (ctx.IssueStepDetailsModel.ContainsKey(issueid))
            {
                ctx.IssueStepDetailsModel[issueid].Add(result);
            }
            else
            {
                List<StepDetails> lst = new List<StepDetails>();
                lst.Add(result);
                ctx.IssueStepDetailsModel.Add(issueid, lst);
            }

            return result.stepId;
        }



        /// <summary>
        /// This method is use to find issue id with the summary in the current cycle.
        /// If the issue is not present in the TestCaseModel yet an API call is used to fetch the issue id.
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="summaryName"></param>
        /// <returns>It will return the first issue with this summaryName in the current cycle or 0 if no issue could be found.</returns>

        private static int FindIssueId(ZephyrContext ctx, string summaryName)
        {
            try
            {
                List<Issues> lstIssues = ctx.TestCaseModel.issues as List<Issues>;
                return lstIssues.Where(s => s.summarydetails.summary == summaryName).Select(s => s.id).FirstOrDefault();
            }
            catch (Exception)
            {
                int issueId = FindIssueId(summaryName);
                ctx.TestCaseModel.issues.Add(new Issues()
                {
                    id = issueId,
                    summarydetails = new summarydetails()
                    {
                        summary = summaryName,
                    },
                });
                return issueId;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="summaryName"></param>
        /// <returns>The first issue id with this summaryName in the given cycle</returns>
        private static int FindIssueId(string summaryName)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method is use to find or create execution details
        /// </summary>
        /// <param name="issueId"></param>
        /// <param name="cycleName"></param>
        /// <param name="versionName"></param>
        /// <param name="method"></param>
        /// <param name="ctx"></param>
        /// <returns>It will return list of executions details.</returns>

        private async static Task<Executions> FindExecutionsDetails(string issueId, string cycleName, string versionName, ZephyrContext ctx)
        {
            var relativeUrl = ZAPIEndpoints.JIRA_ExecutionDetailURL;
            var absoluteUrl = string.Concat(ctx.GetAbsUrl(relativeUrl), issueId);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(absoluteUrl);
            request.Method = "GET";
            request.ContentType = request.MediaType = "application/json";
            request.Headers.Add("Authorization", string.Concat("Basic ", ctx.GetBase64BasicAuth()));
            try
            {
                WebResponse webResponse = await request.GetResponseAsync();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                string response = responseReader.ReadToEnd();
                ExecutionModel jsonresult = JsonConvert.DeserializeObject<ExecutionModel>(response);
                List<Executions> lstexecutions = jsonresult.executions as List<Executions>;
                Executions executions = lstexecutions.Where(e => e.cycleName == cycleName && e.versionName == versionName).FirstOrDefault();
                return executions;
            }
            catch (Exception)
            {
                return new Executions();
            }
        }

        /// <summary>
        /// This method is used for adding a issue to execution cycle.
        /// </summary>
        /// <param name="postData"></param>
        /// <param name="ctx"></param>
        /// <returns>it will return issues execution id for respective cycle.</returns>

        private async static Task<int> AddExecution(byte[] postData, ZephyrContext ctx)
        {
            var relativeUrl = ZAPIEndpoints.JIRA_AddIssuetoExecutionURL;
            var requestUri = ctx.GetAbsUrl(relativeUrl);
            var result = "";
            if (!string.IsNullOrEmpty(requestUri))
            {
                var request = WebRequest.Create(requestUri) as HttpWebRequest;
                if (request != null)
                {
                    request.KeepAlive = true;
                    var cachePolicy = new RequestCachePolicy(RequestCacheLevel.BypassCache);
                    request.CachePolicy = cachePolicy;
                    request.Expect = null;
                    request.Method = "POST";

                    request.ContentType = request.MediaType = "application/json";
                    request.Headers.Add("Authorization", string.Concat("Basic ", ctx.GetBase64BasicAuth()));

                    if (postData != null)
                    {
                        request.ContentLength = postData.Length;
                        using (var dataStream = request.GetRequestStream())
                        {
                            dataStream.Write(postData, 0, postData.Length);
                        }
                    }
                    try
                    {
                        using (var httpWebResponse = await request.GetResponseAsync() as HttpWebResponse)
                        {
                            if (httpWebResponse != null)
                            {
                                using (var streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                                {
                                    result = streamReader.ReadToEnd();
                                    var jobject = JsonConvert.DeserializeObject<JObject>(result);
                                    foreach (var x in jobject)
                                    {
                                        return Convert.ToInt32(x.Key);
                                    }
                                }

                            }
                        }
                    }
                    catch (Exception) { return 0; }
                }
            }
            return 0;
        }

        /// <summary>
        /// This method is used for adding a issue.
        /// </summary>
        /// <param name="postData"></param>
        /// <param name="summary"></param>
        /// <param name="ctx"></param>
        /// <returns>it will return issue id</returns>

        private async static Task<int> AddIssue(byte[] postData, string summary, ZephyrContext ctx)
        {
            var requestUri = ctx.GetAbsUrl(JAPIEndpoints.JIRA_AddIssueURL);
            var result = "";
            if (!string.IsNullOrEmpty(requestUri))
            {
                var request = WebRequest.Create(requestUri) as HttpWebRequest;
                if (request != null)
                {
                    request.KeepAlive = true;
                    var cachePolicy = new RequestCachePolicy(RequestCacheLevel.BypassCache);
                    request.CachePolicy = cachePolicy;
                    request.Expect = null;
                    request.Method = "POST";

                    request.ContentType = request.MediaType = "application/json";
                    request.Headers.Add("Authorization", string.Concat("Basic ", ctx.GetBase64BasicAuth()));

                    if (postData != null)
                    {
                        request.ContentLength = postData.Length;
                        using (var dataStream = request.GetRequestStream())
                        {
                            dataStream.Write(postData, 0, postData.Length);
                        }
                    }

                    try
                    {
                        using (var httpWebResponse = await request.GetResponseAsync() as HttpWebResponse)
                        {
                            if (httpWebResponse != null)
                            {
                                using (var streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                                {
                                    result = streamReader.ReadToEnd();
                                    IssueDetails jsonresult = JsonConvert.DeserializeObject<IssueDetails>(result);
                                    TestCaseModel obj = new TestCaseModel();

                                    summarydetails objsummarydetails = new summarydetails();
                                    Issues objIssues = new Issues();
                                    objsummarydetails.summary = summary;
                                    objIssues.summarydetails = objsummarydetails;
                                    objIssues.id = jsonresult.id;
                                    objIssues.key = jsonresult.key;
                                    ctx.TestCaseModel.issues.Add(objIssues);
                                    return jsonresult.id;
                                }
                            }
                        }
                    }
                    catch (Exception) { return 0; }
                }
            }
            return 0;
        }

        private static int EnsureTestStep(string testStepDetails, int issueid, ZephyrContext ctx)
        {
            //Create json string for add Test Step Details
            dynamic jsonObject = new JObject();
            jsonObject.step = testStepDetails;
            jsonObject.data = string.Empty;
            jsonObject.result = string.Empty;
            string teststepjson = JsonConvert.SerializeObject(jsonObject, Formatting.Indented);

            // Init IsseuStepDetailsModel with an empty list of StepDetails
            if (!ctx.IssueStepDetailsModel.ContainsKey(issueid))
            {
                ctx.IssueStepDetailsModel.Add(issueid, new List<StepDetails>());
            }

            //Add Test Step Details
            List<StepDetails> lst = ctx.IssueStepDetailsModel[issueid];
            int stepId = lst.Where(s => s.stepDetails == testStepDetails).Select(s => s.stepId).FirstOrDefault();

            if (stepId == 0)
            {
                StepDetails result = Task.Run(() => AddTestDetails(issueid, StringToByteArray(teststepjson), ctx)).Result;
                ctx.IssueStepDetailsModel[issueid].Add(result);
                stepId = result.stepId;
            }

            return stepId;
        }

        private static int CreateIssue(string summary, ZephyrContext ctx, params string[] labels)
        {

            List<string> labelList = labels != null ? new List<string>(labels) : new List<string>();
            //Create Json string with parameters
            var lstDp =
                 new field
                 {
                     fields = new fields
                     {
                         project = new project { id = ctx.ProjectId },
                         description = "description",
                         summary = summary,
                         issuetype = new issuetype { id = ctx.TestIssueTypeId },
                         labels = labelList
                     }
                 };

            var json = JsonConvert.SerializeObject(lstDp, Formatting.Indented);

            //Add issue
            int issueid = Task.Run(() => AddIssue(StringToByteArray(json), summary, ctx)).Result;
            return issueid;
        }

        /// <summary>
        /// This method is used for fetching execution cycle id.
        /// </summary>
        /// <param name="ctx"></param>
        /// <returns>It will return cycle id </returns>

        private async static Task<int> FetchCycleId(ZephyrContext ctx)
        {
            var absoluteUrl = string.Format(ctx.GetAbsUrl(ZAPIEndpoints.JIRA_CycleDetailURL), ctx.ProjectId, ctx.ReleaseId);
            int cycleId = 0;
            var request = (HttpWebRequest)WebRequest.Create(absoluteUrl);
            request.Method = "GET";
            request.ContentType = request.MediaType = "application/json";
            request.Headers.Add("Authorization", string.Concat("Basic ", ctx.GetBase64BasicAuth()));
            try
            {
                WebResponse webResponse = await request.GetResponseAsync();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                string response = responseReader.ReadToEnd();
                var jobject = JsonConvert.DeserializeObject<JObject>(response);
                foreach (var x in jobject)
                {
                    JToken value = x.Value;
                    CycleDetails jsonresult = JsonConvert.DeserializeObject<CycleDetails>(Convert.ToString(x.Value));

                    if (jsonresult.name == ctx.TestCycleName)
                    {
                        cycleId = Convert.ToInt32(x.Key);
                        return cycleId;
                    }
                }
            }
            catch (Exception)
            {
                return 0;
            }
            return 0;
        }

        /// <summary>
        /// This method is used for converting string to byte array
        /// </summary>
        /// <param name="str"></param>
        /// <returns>It will return converted string byte array</returns>
        
        private static byte[] StringToByteArray(string str)
        {
            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
            return enc.GetBytes(str);
        }

        /// <summary>
        /// This method is used for adding test step details
        /// </summary>
        /// <param name="issueId"></param>
        /// <param name="method"></param>
        /// <param name="postData"></param>
        /// <param name="ctx"></param>
        /// <returns>It will return step details object</returns>

        private async static Task<StepDetails> AddTestDetails(int issueId, byte[] postData, ZephyrContext ctx)
        {
            var relativeUrl = ZAPIEndpoints.JIRA_TestStepDetailsUpdate;
            var absoluteUrl = string.Concat(ctx.GetAbsUrl(relativeUrl), Convert.ToString(issueId));
            if (!string.IsNullOrEmpty(absoluteUrl))
            {
                var request = WebRequest.Create(absoluteUrl) as HttpWebRequest;
                if (request != null)
                {
                    request.KeepAlive = true;
                    var cachePolicy = new RequestCachePolicy(RequestCacheLevel.BypassCache);
                    request.CachePolicy = cachePolicy;
                    request.Expect = null;
                    request.Method = "POST";

                    request.ContentType = request.MediaType = "application/json";
                    request.Headers.Add("Authorization", string.Concat("Basic ", ctx.GetBase64BasicAuth()));

                    if (postData != null)
                    {
                        request.ContentLength = postData.Length;
                        using (var dataStream = request.GetRequestStream())
                        {
                            dataStream.Write(postData, 0, postData.Length);
                        }
                    }
                    try
                    {
                        using (var httpWebResponse = await request.GetResponseAsync() as HttpWebResponse)
                        {
                            if (httpWebResponse != null)
                            {
                                using (var streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                                {
                                    string response = streamReader.ReadToEnd();
                                    StepDetails jsonresult = JsonConvert.DeserializeObject<StepDetails>(response);
                                    return jsonresult;
                                }
                            }
                        }
                    }

                    catch (Exception)
                    {

                        return new StepDetails();
                    }
                }
            }
            return new StepDetails(); ;
        }

        /// <summary>
        /// This method is used for fetching steps details for specific issue id.
        /// </summary>
        /// <param name="issueId"></param>
        /// <param name="method"></param>
        /// <param name="ctx"></param>
        /// <returns>It will return list of test steps</returns>

        private async static Task<TestSteps> GetIssueTestSteps(int issueId, ZephyrContext ctx)
        {
            var relativeUrl = ZAPIEndpoints.JIRA_TestStepDetailsUpdate;
            var absoluteUrl = string.Concat(ctx.GetAbsUrl(relativeUrl), Convert.ToString(issueId));
            var request = (HttpWebRequest)WebRequest.Create(absoluteUrl);
            request.Method = "GET";
            request.ContentType = request.MediaType = "application/json";
            request.Headers.Add("Authorization", string.Concat("Basic ", ctx.GetBase64BasicAuth()));
            try
            {
                WebResponse webResponse = await request.GetResponseAsync();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                string response = responseReader.ReadToEnd();
                TestSteps jsonresult = JsonConvert.DeserializeObject<TestSteps>(response);
                return jsonresult;
            }
            catch (Exception)
            {
                return new TestSteps();
            }
        }

        /// <summary>
        /// This method is used for fetching steps result for specific execution id.
        /// </summary>
        /// <param name="executionId"></param>
        /// <param name="ctx"></param>
        /// <returns>It will return list of steps reult</returns>
        private async static Task<List<StepsResult>> GetStepsResult(int executionId, ZephyrContext ctx)
        {
            var relativeUrl = ZAPIEndpoints.JIRA_GETStepsResult;
            var absoluteUrl = string.Concat(ctx.GetAbsUrl(relativeUrl), executionId);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(absoluteUrl);
            request.Method = "GET";
            request.ContentType = request.MediaType = "application/json";
            request.Headers.Add("Authorization", string.Concat("Basic ", ctx.GetBase64BasicAuth()));
            try
            {
                WebResponse webResponse = await request.GetResponseAsync();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                string response = responseReader.ReadToEnd();
                List<StepsResult> jsonresult = JsonConvert.DeserializeObject<List<StepsResult>>(response);
                return jsonresult;
            }
            catch (Exception)
            {
                return new List<StepsResult>();
            }
        }

        /// <summary>
        /// This method adds an execution of an existing test issue to the test cycle.
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="cycleid"></param>
        /// <param name="issueid"></param>
        /// <param name="stepId"></param>
        /// <returns>It will return step execution object.</returns>
        
        private static ExecutionDetails AddTestExecution(ZephyrContext ctx, int cycleid, int issueid, int stepId = 0)
        {
            dynamic jsonObject = new JObject();
            jsonObject.assigneeType = "assignee";
            jsonObject.assignee = ctx.DefaultAssignee;
            jsonObject.cycleId = cycleid;
            jsonObject.issueId = issueid;
            jsonObject.projectId = ctx.ProjectId;
            jsonObject.versionId = ctx.ReleaseId;
            string json = JsonConvert.SerializeObject(jsonObject, Formatting.Indented);

            var executionid = Task.Run(() => AddExecution(StringToByteArray(json), ctx)).Result;

            return new ExecutionDetails()
            {
                stepId = stepId,
                issueId = issueid,
                executionId = executionid,
            };
        }

         #endregion
    }

}