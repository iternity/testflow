﻿using Iternity.TestFlow.Zephyr.Model;
using Iternity.TestFlow.Zephyr.Internal;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;

namespace Iternity.TestFlow.Zephyr
{
    public class ZephyrConnector : ITestManagementConnector
    {
        private ZephyrContext ctx;

        public ZephyrConnector(ZephyrContext ctx)
        {
            ctx.Validate();
            this.ctx = ctx;
        }

        public void EnsureTestStepExecution(TestStepInfo testStepInfo)
        {
            JiraZAPI.EnsureTestStepExecution(ctx, testStepInfo.TestName, testStepInfo.TestStepName, testStepInfo.Tags);
        }

        public void UpdateTestStepExecutionResult(TestStepInfo testStepInfo)
        {
            ExecutionDetails stepExecution = JiraZAPI.EnsureTestStepExecution(ctx, testStepInfo.TestName, testStepInfo.TestStepName, testStepInfo.Tags);

            string result = $"{testStepInfo.Error}"; // if error is null this string is empty
            int status = ZAPITestStepExecutionStatus.GetStatus(testStepInfo.Status);

            bool flag = Task.Run(() => JiraZAPI.UpdateTestStepResult(ctx, stepExecution, status, result)).Result;
        }

        public void UpdateTestExecutionResult(TestInfo testInfo)
        {
            ExecutionDetails executionDetails = JiraZAPI.EnsureTestExecution(this.ctx, testInfo.TestName, testInfo.Tags);

            string testExecutionId = Convert.ToString(executionDetails.executionId);
            int status = ZAPITestExecutionStatus.GetStatus(testInfo.Status);

            bool results = Task.Run(() => JiraZAPI.UpdateExecutionStatus(testExecutionId, status, "Created from automation script", ctx)).Result;
        }

    }

    public class ZephyrContext
    {
        public string JiraBaseUrl { private get; set; }
        public string JiraUser { private get; set; }
        public string JiraPassword { private get; set; }
        public string DefaultAssignee { get; set; }
        public int ProjectId { get; set; }
        public int ReleaseId { get; set; }
        public string ReleaseName { get; set; }
        public string TestCycleName { get; set; }
        public int TestIssueTypeId { get; set; }

        /// <summary>
        /// Loads and resets the test issue with the given <paramref name="testName"/> from JIRA/Zephyr into a <see cref="TestCaseModel"/>.
        /// If the issue could not be found an empty <see cref="TestCaseModel"/> is created.
        /// Only call this method after all the properties of this <see cref="ZephyrContext"/> has been setup.
        /// This method validates the state of this <see cref="ZephyrContext"/>.
        /// </summary>
        /// <param name="testName"></param>
        public void InitTestCaseModel(string testName)
        {
            Validate();
            TestCaseModel = Task.Run(() => JiraZAPI.GetTestCase(this, testName)).Result;
            if (TestCaseModel.issues.Capacity > 0)
            {
                // Reset all test steps if the test case already exists in JIRA/Zephyr
                var executionDetails = JiraZAPI.EnsureTestExecution(this, testName);
                JiraZAPI.ResetAllTestStepResults(this, executionDetails).GetAwaiter().GetResult();
            }
        }

        internal TestCaseModel TestCaseModel { get; private set; } = new TestCaseModel();

        internal Dictionary<int, List<StepDetails>> IssueStepDetailsModel { get; private set; } = new Dictionary<int, List<StepDetails>>();
        

        internal string GetAbsUrl(string relativeUrl)
        {
            return string.Concat(JiraBaseUrl, relativeUrl);
        }

        internal string GetBase64BasicAuth()
        {
            string usernamePassword = string.Concat(JiraUser, ":", JiraPassword);
            return Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(usernamePassword));
        }

        internal void Validate()
        {
            ValidateNotEmpty("JiraBaseUrl", JiraBaseUrl);
            ValidateUrl("JiraBaseUrl", JiraBaseUrl);
            ValidateNotEmpty("JiraUser", JiraUser);
            ValidateNotEmpty("JiraPassword", JiraPassword);
            ValidateNotEmpty("ProjectId", ProjectId);
            ValidateNotEmpty("ReleaseId", ReleaseId);
            ValidateNotEmpty("ReleaseName", ReleaseName);
            ValidateNotEmpty("TestCycleName", TestCycleName);
            ValidateNotEmpty("TestIssueTypeId", TestIssueTypeId);
        }

        private void ValidateUrl(string propertyName, string value)
        {
            bool isUri = Uri.TryCreate(value, UriKind.Absolute, out Uri uriResult);
            bool isHttp = uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps;
            if (!isUri || !isHttp)
            {
                throw new InvalidZephyrContextException(string.Format("Property '{0}' is not an URL. Current value: '{1}'", propertyName, value));
            }
        }

        private void ValidateNotEmpty(string propertyName, object value)
        {
            string sValue = $"{value}";
            if (string.IsNullOrEmpty(sValue))
            {
                throw new InvalidZephyrContextException(string.Format("Property '{0}' should not be empty", propertyName));
            }
        }

    }

    public class InvalidZephyrContextException : Exception
    {
        public InvalidZephyrContextException(string message) : base(message)
        {
        }
    }
}
