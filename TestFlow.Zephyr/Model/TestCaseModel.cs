﻿/// <summary>
/// This is model class for holding JIRA test case details.
/// </summary>
/// <author>Aress QA</author>

using Newtonsoft.Json;
using System.Collections.Generic;

namespace Iternity.TestFlow.Zephyr.Model
{
    /// <summary>
    /// This is model class for holding JIRA/Zephyr test case details and list of issues .- fetched from JIRA REST API
    /// </summary>
    public class TestCaseModel
    {
        public string expand { get; set; }
        public int startAt { get; set; }
        public int maxResults { get; set; }
        public int total { get; set; }

        [JsonProperty("issues")]
        public List<Issues> issues { get; set; } = new List<Issues>();
    }

    /// <summary>
    /// This is model class for holding JIRA/Zephyr issue details,list of summary and list of step details.- fetched from JIRA REST API
    /// </summary>
    public class Issues
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("key")]
        public string key { get; set; }

        [JsonProperty("self")]
        public string self { get; set; }

        [JsonProperty("fields")]
        public summarydetails summarydetails { get; set; }

        public  List<StepDetails> stepDetails { get; set; } = new List<StepDetails>();
    }

    /// <summary>
    /// This is model class for holding JIRA/Zephyr step details .- fetched from JIRA REST API
    /// </summary>
    public class StepDetails
    {
        [JsonProperty("id")]
        public int stepId { get; set; }

        [JsonProperty("step")]
        public string stepDetails { get; set; }
    }

    /// <summary>
    /// This is model class for holding JIRA/Zephyr summary details .- fetched from JIRA REST API
    /// </summary>
    
    public class summarydetails
    {
        [JsonProperty("summary")]
        public string summary { get; set; }
    }

    /// <summary>
    /// This is model class for holding JIRA/Zephyr cycle details .- fetched from JIRA REST API
    /// </summary>
    
    public class CycleDetails
    {
        [JsonProperty("name")]
        public string name { get; set; }

        [JsonProperty("versionId")]
        public int versionId { get; set; }

        [JsonProperty("versionName")]
        public string versionName { get; set; }
    }

    /// <summary>
    /// This is model class for holding JIRA/Zephyr issue and list of step details.- fetched from JIRA REST API
    /// </summary>
    
    public class IssueStepDetails {
        public int issueId { get; set; }
        public List<StepDetails> stepDetails { get; set; } = new List<StepDetails>();
    }

    /// <summary>
    /// This is model class for holding lis of JIRA/Zephyr Step Execution details.- fetched from JIRA REST API
    /// </summary>
    
    public class TestSteps {
        [JsonProperty("stepBeanCollection")]
        public List<StepDetails> stepDetails { get; set; } = new List<StepDetails>();
    }

    /// <summary>
    /// This is model class for holding JIRA/Zephyr Step Execution details.- fetched from JIRA REST API
    /// </summary>
    
    public class ExecutionDetails {
        public int stepId { get; set; }
        public int issueId { get; set; }
        public int executionId { get; set; }
    }
   
}
