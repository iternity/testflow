﻿/// <summary>
/// This is model class for holding JIRA execution details.
/// </summary>
/// <author>Aress QA</author>

using Newtonsoft.Json;
using System.Collections.Generic;

namespace Iternity.TestFlow.Zephyr.Model
{
    /// <summary>
    /// This is model class for holding list of JIRA execution details.- fetched from JIRA REST API
    /// </summary>
    public class ExecutionModel
    {
        public int issueId { get; set; }

        [JsonProperty("executions")]
        public List<Executions> executions { get; set; }
    }

    /// <summary>
    /// This is model class for holding JIRA execution details - fetched from JIRA REST API
    /// </summary>
    public class Executions
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("cycleId")]
        public int cycleId { get; set; }

        [JsonProperty("cycleName")]
        public string cycleName { get; set; }

        [JsonProperty("versionId")]
        public int versionId { get; set; }

        [JsonProperty("versionName")]
        public string versionName { get; set; }

        [JsonProperty("projectId")]
        public int projectId { get; set; }
    }

    /// <summary>
    /// This is model class for holding list of bug details. - fetched from JIRA REST API 
    /// </summary>
    public class ExecutionsBug
    {
        [JsonProperty("0")]
        public BugDetails details { get; set; }
    }

    /// <summary>
    /// This is model class for holding bug details.
    /// </summary>
    public class BugDetails
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("orderId")]
        public int orderId { get; set; }

        [JsonProperty("cycleId")]
        public int cycleId { get; set; }

        [JsonProperty("executionStatus")]
        public int executionStatus { get; set; }
    }

    /// <summary>
    /// This is model class for holding issue details.- fetched from JIRA REST API
    /// </summary>
    public class IssueDetails
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("key")]
        public string key { get; set; }
    }
}
