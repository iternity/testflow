﻿/// <summary>
/// This is model class for holding JIRA issues details.
/// </summary>
/// <author>Aress QA</author>

using Newtonsoft.Json;
using System.Collections.Generic;

namespace Iternity.TestFlow.Zephyr.Model
{
    /// <summary>
    /// This is model class for holding JIRA/Zephyr issue details .- fetched from JIRA REST API
    /// </summary>
    public class fields
    {
        public project project { get; set; }

        public string summary { get; set; }

        public string description { get; set; }

        public issuetype issuetype { get; set; }

        public List<string> labels { get; set; }
    }

    /// <summary>
    /// This is model class for holding JIRA/Zephyr project id .- fetched from JIRA REST API
    /// </summary>
    public class project
    {
        public int id { get; set; }
    }

    /// <summary>
    /// This is model class for holding JIRA/Zephyr issue type .- fetched from JIRA REST API
    /// </summary>
    public class issuetype
    {
        public int id { get; set; }
    }

    /// <summary>
    /// This is model class for holding JIRA/Zephyr issue fields .- fetched from JIRA REST API
    /// </summary>
    public class field
    {
        public fields fields { get; set; }
    }

    /// <summary>
    /// This is model class for holding JIRA/Zephyr test step results .- fetched from JIRA REST API
    /// </summary>
    public class TestsStepsResult
    {
        [JsonProperty("0")]
        public StepsResult stepsResults { get; set; }
    }

    /// <summary>
    /// This is model class for holding JIRA/Zephyr step results .- fetched from JIRA REST API
    /// </summary>
    public class StepsResult
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("status")]
        public string status { get; set; }

        [JsonProperty("stepId")]
        public int stepId { get; set; }

        [JsonProperty("step")]
        public string step { get; set; }

        [JsonProperty("orderId")]
        public int orderId { get; set; }

    }
}