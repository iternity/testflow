﻿using System;
using System.Collections.Generic;
using System.Globalization;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Bindings;
using TechTalk.SpecFlow.Bindings.Reflection;
using TechTalk.SpecFlow.BindingSkeletons;
using TechTalk.SpecFlow.Configuration;
using TechTalk.SpecFlow.Tracing;

namespace Iternity.TestFlow.SpecFlowPlugin
{
    public class HookableTestTracer : TestTracer, ITestTracer
    {
        public ITestTracer TestTracerCallbacks { private get; set; }

        public HookableTestTracer(ITraceListener traceListener, IStepFormatter stepFormatter, 
            IStepDefinitionSkeletonProvider stepDefinitionSkeletonProvider, SpecFlowConfiguration specFlowConfiguration) 
            : base(traceListener, stepFormatter, stepDefinitionSkeletonProvider, specFlowConfiguration)
        {
        }

        void ITestTracer.TraceBindingError(BindingException ex)
        {
            TraceBindingError(ex);
            TestTracerCallbacks.TraceBindingError(ex);
        }

        void ITestTracer.TraceDuration(TimeSpan elapsed, IBindingMethod method, object[] arguments)
        {
            TraceDuration(elapsed, method, arguments);
            TestTracerCallbacks.TraceDuration(elapsed, method, arguments);
        }

        void ITestTracer.TraceDuration(TimeSpan elapsed, string text)
        {
            TraceDuration(elapsed, text);
            TestTracerCallbacks.TraceDuration(elapsed, text);
        }

        void ITestTracer.TraceError(Exception ex, TimeSpan ts)
        {
            TraceError(ex, ts);
            TestTracerCallbacks.TraceError(ex, ts);
        }

        void ITestTracer.TraceNoMatchingStepDefinition(StepInstance stepInstance, ProgrammingLanguage targetLanguage, CultureInfo bindingCulture, List<BindingMatch> matchesWithoutScopeCheck)
        {
            TraceNoMatchingStepDefinition(stepInstance,targetLanguage, bindingCulture, matchesWithoutScopeCheck);
            TestTracerCallbacks.TraceNoMatchingStepDefinition(stepInstance, targetLanguage, bindingCulture, matchesWithoutScopeCheck);
        }

        void ITestTracer.TraceStep(StepInstance stepInstance, bool showAdditionalArguments)
        {
            TraceStep(stepInstance, showAdditionalArguments);
            TestTracerCallbacks.TraceStep(stepInstance, showAdditionalArguments);
        }

        void ITestTracer.TraceStepDone(BindingMatch match, object[] arguments, TimeSpan duration)
        {
            TraceStepDone(match, arguments, duration);
            TestTracerCallbacks.TraceStepDone(match, arguments, duration);
        }

        void ITestTracer.TraceStepPending(BindingMatch match, object[] arguments)
        {
            TraceStepPending(match, arguments);
            TestTracerCallbacks.TraceStepPending(match, arguments);
        }

        void ITestTracer.TraceStepSkipped()
        {
            TraceStepSkipped();
            TestTracerCallbacks.TraceStepSkipped();
        }

        void ITestTracer.TraceWarning(string text)
        {
            TraceWarning(text);
            TestTracerCallbacks.TraceWarning(text);
        }
    }
}
