﻿using System;
using System.Collections.Generic;
using System.Globalization;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Bindings;
using TechTalk.SpecFlow.Bindings.Reflection;
using TechTalk.SpecFlow.Tracing;

namespace Iternity.TestFlow.SpecFlowPlugin
{
    /// <summary>
    /// Empty implementation of <see cref="ITestTracer"/>.
    /// Simply override the methods you need and pass it to <see cref="HookableTestTracer"/>
    /// to hook in the normen <see cref="TestTracer"/>.
    /// </summary>
    public class TestTracerCallbacks : ITestTracer
    {
        public virtual void TraceBindingError(BindingException ex)
        {
        }

        public virtual void TraceDuration(TimeSpan elapsed, IBindingMethod method, object[] arguments)
        {
        }

        public virtual void TraceDuration(TimeSpan elapsed, string text)
        {
        }

        public virtual void TraceError(Exception ex, TimeSpan ts)
        {
        }

        public virtual void TraceNoMatchingStepDefinition(StepInstance stepInstance, ProgrammingLanguage targetLanguage, CultureInfo bindingCulture, List<BindingMatch> matchesWithoutScopeCheck)
        {
        }

        public virtual void TraceStep(StepInstance stepInstance, bool showAdditionalArguments)
        {
        }

        public virtual void TraceStepDone(BindingMatch match, object[] arguments, TimeSpan duration)
        {
        }

        public virtual void TraceStepPending(BindingMatch match, object[] arguments)
        {
        }

        public virtual void TraceStepSkipped()
        {
        }

        public virtual void TraceWarning(string text)
        {
        }
    }
}
