# Test Flow
The project provides modules to synchronize test steps and test results of an automatic test run 
to an external test management tool.

The core idea is to enable a test first approach, where all tests are defined as executable code, 
even if they are not fully automated yet but needs a human tester.

The whole process looks like this:
 
1. Define all test scenarios as code (e.g. MSTest, NUnit or SpecFlow). 
    As long as you can't provide a full test automation use stubs to "declare" tests and test steps.
1. Use *TestFlow* components to connect your test execution to an external test management tool.
1. Execute all tests as part of an automated build or CI process.
1. Go to the test management tool and see all tests and their respective results and status. 
1. Manually execute those tests that are not yet executed by the automated build or CI process.
1. Use your test management tool to create a final test report, including automated and manual test executions.

### How can Business Analysts and POs make use of this?
BAs and POs are normally not providing test cases as executable code.
In this case you can make use of BDD (Gherkin/Cucumber/SpecFlow) to define test scenarios in a "Given ... when ... then ..." format and then generate code behind automatically.

Please see the *SpecFlowExample* for more details. The *TestReportHooks.cs* class provides code to connect the SpecFlow test step execution to JIRA/Zephyr.
Please make sure to define your own *.runsettings* and connect your test environment to it (in VS 2017 its *Test->TestSettings->Select Test Settings file*).
Alternativaly you can define the test settings as environment variabes. See *TestReportHooks.cs* ctor.

## Projects

| Project           | Type      | Description | 
| :-------------     |:---------:|:-------------|
| *TestFlow*        | Component | Provides the core abstraction *ITestManagementConnector* and a dummy implementation of this interface that prints out to the console. |
| *TestFlow.Zephyr* | Component | Provides an implementation of the *ITestManagementConnector* interface for JIRA/Zephyr (using the Zephyr Rest API "ZAPI"). |
| *SpecFlowExample* | Example   | A working example that hooks into the *SpecFlow* test execution process and uses the *TestFlow.Zephyr* Connector to sync up with JIRA/Zephyr. Please make sure to define your own *.runsettings* and connect your test environment to it (in VS 2017 its *Test->TestSettings->Select Test Settings file*). Alternativaly you can define the test settings as environment variabes. See *TestReportHooks.cs* ctor. |