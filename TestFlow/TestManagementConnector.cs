using System;

namespace Iternity.TestFlow
{
    /// <summary>
    /// A <see cref="ITestManagementConnector"/> provides simple methods to create and update test execution results.
    /// Possible implementations of this interface could provide a simple text based report or a connection to a test management plattform like Zephyr.
    /// </summary>
    public interface ITestManagementConnector
    {
        /// <summary>
        /// Ensures that a test step execution exists regarding the given <paramref name="testStepInfo"/>.
        /// The status of the test step is NOT updated by this method.
        /// </summary>
        /// <param name="testStepInfo"></param>
        void EnsureTestStepExecution(TestStepInfo testStepInfo);

        /// <summary>
        /// Updates the result of a test step execution regarding the given <paramref name="testStepInfo"/>.
        /// The method makes sure that an appropriate test step execution exists in the target report.
        /// </summary>
        /// <param name="testInfo"></param>
        void UpdateTestStepExecutionResult(TestStepInfo testStepInfo);

        /// <summary>
        /// Updates the result of a whole test execution regarding the given <paramref name="testInfo"/>.
        /// The method makes sure that an appropriate test execution exists in the target report.
        /// </summary>
        /// <param name="testInfo"></param>
        void UpdateTestExecutionResult(TestInfo testInfo);
    }

    /// <summary>
    /// The Dummy implementation of the <see cref="ITestManagementConnector"/> just prints out to the console.
    /// </summary>
    public class ConsoleConnector : ITestManagementConnector
    {
        public void EnsureTestStepExecution(TestStepInfo testStepInfo)
        {
            Console.WriteLine("Created Test Step Execution for '" + testStepInfo.TestStepName + "' to '" + testStepInfo.Status + "'");
        }
        public void UpdateTestStepExecutionResult(TestStepInfo testStepInfo)
        {
            Console.WriteLine("Update Test Step Execution Result for '" + testStepInfo.TestStepName + "' to " + testStepInfo.Status + "'");
        }

        public void UpdateTestExecutionResult(TestInfo testInfo)
        {
            Console.WriteLine("Update Test Execution Result for '" + testInfo.TestName + "' to '" + testInfo.Status + "'");
        }
    }

    /// <summary>
    /// A <see cref="TestInfo"/> holds the relevant information about the test and its state.
    /// </summary>
    public class TestInfo
    {
        public string TestName { get; private set; }
        public Exception Error { get; set; }
        public TestExecutionStatus Status { get; set; }
        public string[] Tags { get; set; }

        public TestInfo(string testName)
        {
            TestName = testName;
        }
    }

    /// <summary>
    /// A <see cref="TestStepInfo"/> holds the relevant information about the a test step test and its state.
    /// </summary>
    public class TestStepInfo : TestInfo
    {
        public TestStepInfo(string testName, string testStepName) : base(testName)
        {
            TestStepName = testStepName;
        }

        public string TestStepName { get; private set; }
    }

    /// <summary>
    /// For conveniance we use a single model for execution status of tests and single test steps.
    /// This enum should be kept small so that it can be converted from and to vendor specific implementations (e.g. Zephyr and SpecFlow).
    /// </summary>
    public enum TestExecutionStatus
    {
        Unexecuted = 0,
        Pass = 1,
        Fail = 2,
    }

}
